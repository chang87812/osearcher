package com.omall.search.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 系统级拦截器类
 * 
 * @ClassName:AuthorityInterceptor
 * @Description:
 * @date: 2014-10-23下午12:03:40
 * @author: 谢洪飞
 * @version: V1.0
 */
public class AuthorityInterceptor extends HandlerInterceptorAdapter {

	/**
	 * 完成页面的render后调用
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception exception) throws Exception {
	}

	/**
	 * 在调用controller具体方法后拦截
	 */
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView) throws Exception {
		if(modelAndView==null){
			modelAndView=new ModelAndView();
		}	
		modelAndView.getModel().put("path", request.getContextPath());
	}

	/**
	 * 在调用controller具体方法前拦截
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {
		return true;
	}
}
