package com.omall.search.dao;

import java.util.List;

/**
 *   查询索引源数据Dao接口
 *  
 * @ClassName:BaseDao
 * @Description:查询索引源数据Dao接口
 * @date: 2014-10-23下午06:49:28
 * @author: 谢洪飞
 * @version: V1.0
 */
public interface BaseDao {

	
	/**
	 *  查询索引源数据集合
	 * 
	 *@Title:queryForList
	 *@Description:     查询索引源数据集合
	 *@param:@param 
	 *       statementName 要检索的Mapper Id 
	 *@param:@return
	 *@return:List 查询结果集合
	 *@author:  谢洪飞
	 *@Thorws:
	 */
	@SuppressWarnings("unchecked")
	public List queryForList(String statementName);
	
	public static final String BASEDAO = "baseDao";
}
