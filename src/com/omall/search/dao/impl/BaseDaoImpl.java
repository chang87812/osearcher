package com.omall.search.dao.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.omall.search.dao.BaseDao;


/**
 *   查询索引源数据Dao实现
 *  
 * @ClassName:BaseDaoImpl
 * @Description:查询索引源数据Dao接口
 * @date: 2014-10-23下午06:49:28
 * @author: 谢洪飞
 * @version: V1.0
 */
@Repository(BaseDao.BASEDAO)
public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SqlSessionTemplate sqlSessionTemplate;
	
	/**
	 *  查询索引源数据集合
	 * 
	 *@Title:queryForList
	 *@Description:     查询索引源数据集合
	 *@param:@param 
	 *       statementName 要检索的Mapper Id 
	 *@param:@return
	 *@return:List 查询结果集合
	 *@author:  谢洪飞
	 *@Thorws:
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List queryForList(String statementName) {
		
		return sqlSessionTemplate.selectList(statementName);
	}
  
	
}
