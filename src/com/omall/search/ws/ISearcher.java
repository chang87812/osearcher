package com.omall.search.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;
import com.omall.search.bean.ReturnTool;

/**
 * 提供外部查询 web service 接口
 * 
 * @ClassName:ISearcher
 * @Description:lucene查询 web service接口
 * @date: 2014-10-23上午11:42:13
 * @author: 谢洪飞
 * @version: V1.0
 */
@WebService
public interface ISearcher {

	
	/**
	 *   web service 实现  Lucene 索引查询方法
	 * 
	 *@Title:luceneSearch
	 *@Description: 实现Lucene查询硬盘索引
	 *@param:@param keyWord 输入的内容
	 *@param:@param fields  要检索的索引库
	 *@param:@param key     要检索的列
	 *@param:@param page    查询的页数
	 *@param:@param row     每页显示条数
	 *@param:@return     
	 *@return:ReturnTool    返回工具类
	 *@author:   谢洪飞
	 *@Thorws:
	 */
	@WebMethod
	public ReturnTool luceneSearch(String keyWord, String fields, String key, Integer page, Integer row);
	
	
	
	/**
	 *  web service 实现 Lucene 索引店铺查询方法
	 * 
	 *@Title:luceneStoreSearcher
	 *@Description:店铺查询
	 *@param:@param keyWord
	 *@param:@param fields
	 *@param:@param key
	 *@param:@param page
	 *@param:@param row
	 *@param:@return
	 *@return:ReturnTool
	 *@author: 谢洪飞
	 *@Thorws:
	 */
	@WebMethod
	public ReturnTool luceneStoreSearcher(String keyWord, String fields, String key,
			Integer page, Integer row);
	
	/**
	 *  测试创建索引
	 * 
	 *@Title:createAndUpdateIndex
	 *@Description:TODO
	 *@param:@return
	 *@Return:boolean
	 *@author: 谢洪飞
	 *@Thorws:
	 */
	public boolean createAndUpdateIndex();

}
