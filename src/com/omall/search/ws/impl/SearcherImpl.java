package com.omall.search.ws.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.lucene.search.IndexSearcher;
import com.omall.search.bean.ReturnTool;
import com.omall.search.bean.Search;
import com.omall.search.exception.ExceptionUtil;
import com.omall.search.service.LuceneService;
import com.omall.search.ws.ISearcher;


/**
 * 提供外部查询 web service 接口(实现)
 * 
 * @ClassName:SearcherImpl
 * @Description:lucene查询 web service接口
 * @date: 2014-10-23上午11:42:13
 * @author: 谢洪飞
 * @version: V1.0
 */
public class SearcherImpl implements ISearcher{

	public Logger logger =  Logger.getLogger(SearcherImpl.class);
	
	//@Autowired
	@Resource
   private LuceneService lunLuceneService;
	
	
	
	public LuceneService getLunLuceneService() {
		return lunLuceneService;
	}

	public void setLunLuceneService(LuceneService lunLuceneService) {
		this.lunLuceneService = lunLuceneService;
	}

	/**
	 *   web service 实现  Lucene 索引查询方法
	 * 
	 *@Title:luceneSearch
	 *@Description: 实现Lucene查询硬盘索引
	 *@param:@param keyWord 输入的内容
	 *@param:@param fields  要检索的索引库
	 *@param:@param key     要检索的列
	 *@param:@param page    查询的页数
	 *@param:@param row     每页显示条数
	 *@param:@return     
	 *@return:ReturnTool    返回工具类
	 *@author:   谢洪飞
	 *@Thorws:
	 */
	@Override
	public ReturnTool luceneSearch(String keyWord, String fields, String key,
			Integer page, Integer row)
	{
		ReturnTool returnTool = new ReturnTool();
		
		int pages = 0 ;
		int counts = 0 ;
		List<Search> searchResult = null;
		 String [] f = key.split(",");
		try 
		  {
			if ( null != keyWord && !"".equals(keyWord))
			   {
				 if ( null != fields && fields.split(",").length > 1)
				    {
					  IndexSearcher indexSearcher = lunLuceneService.getSearchers(fields);
					  searchResult = lunLuceneService.queryFromIndex(indexSearcher, keyWord, f, page, row);
				     
				      counts = lunLuceneService.count(indexSearcher, keyWord, f); 
				    }
				 else
				   {
					   IndexSearcher indexSearcher = lunLuceneService.getSearcher(fields);
					   //String[] f = key.split(",");
					   searchResult = lunLuceneService.queryFromIndex(indexSearcher, keyWord, f, page, row);
					   counts = lunLuceneService.count(indexSearcher, keyWord, f);
				   }
			   }
			pages = (int)Math.ceil((double)counts/row) ;
			returnTool.setPages(pages);
			returnTool.setSuccess(true);
			returnTool.setSearches(searchResult);
			returnTool.setCounts(counts);
		   }
		catch (Exception e)
		  {
			e.printStackTrace();
			String errorMsg = ExceptionUtil.getExceptionMessage(e);
			logger.info(errorMsg);
			returnTool.setSuccess(false);
			returnTool.setMsg(errorMsg);
		  } 
		
		return returnTool;
	}
	
	/**
	 *  web service 实现 Lucene 索引店铺查询方法
	 * 
	 *@Title:luceneStoreSearcher
	 *@Description:店铺查询
	 *@param:@param keyWord
	 *@param:@param fields
	 *@param:@param key
	 *@param:@param page
	 *@param:@param row
	 *@param:@return
	 *@return:ReturnTool
	 *@author: 谢洪飞
	 *@Thorws:
	 */
	public ReturnTool luceneStoreSearcher(String keyWord, String fields, String key,
			Integer page, Integer row){
		ReturnTool returnTool = new ReturnTool();
		return returnTool;
	}
	
	
	/**
	 *  测试创建索引
	 * 
	 *@Title:createAndUpdateIndex
	 *@Description: 创建索引
	 *@param:@return
	 *@Return:boolean
	 *@author: 谢洪飞
	 *@Thorws:
	 */
	public boolean createAndUpdateIndex(){
		boolean flg = false;
		
		try 
		{
		   lunLuceneService.emptyIndex();
		   lunLuceneService.buildIndex();
		   flg = true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			flg = false;
		}
		
		return flg;
	}

}
