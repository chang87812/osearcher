package com.omall.search.util;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

/**
 *   JSON工具类
 *  
 *@Title: JSONUtil.java
 *@Description: 操作JSON数据之间转换
 *@Date: 2014-10-17上午10:54:14
 *@author: 谢洪飞
 *@Version: 1.0
 */
public class JSONUtil {

	
	
 /**
  *  将Object对象转为JSON格式字符串
  * 
  * @Title: getJson
  * @Description: 将Object对象转为JSON格式字符串
  * @Param: @param  o Object对象，如:Map对象
  * @Param: @return JSON格式字符串
  * @Return: String jo.toString();
  * @Author: 谢洪飞
  * @Thorws:
  */
	public static String getJson(Object o){
	    JSONObject jo=JSONObject.fromObject(o);
	    return jo.toString();
	}

  /**
   *  将List集合转JSON字符串
   * 
   * @Title: getJson
   * @Description: 将List集合转JSON字符串
   * @Param: @param list
   * @Param: @return JSON格式字符串
   * @Return: String ja.toString;
   * @Author: 谢洪飞
   * @Thorws:
   */
	public static String getJson(List<?> list){
	    JSONArray ja=JSONArray.fromObject(list);
	    return ja.toString();
	}

	/**
	 * ��Java��������ת��Ϊ�ַ�
	 * @param arry
	 * @return
	 */
	public static String getJson(Object[] arry){
	    JSONArray ja=JSONArray.fromObject(arry);
	    return ja.toString();
	}

	/**
	 * ��json��ʽ���ַ�ת��ΪMap����
	 * @param s
	 * @return
	 */
	public static Map<?, ?> getObject(String s){
	    return	JSONObject.fromObject(s);
	}

	/**
	 * ��json��ʽ���ַ�ת��ΪList����
	 * @param s
	 * @return
	 */
	public static List<?> getArray(String s){
	    return JSONArray.fromObject(s);
	}

	/**
	 * ��json��ʽ���ַ�ת��Ϊĳ��Bean
	 * @param s
	 * @param cls
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Object getObject(String s, Class cls){
	    JSONObject jo=JSONObject.fromObject(s);
	    return JSONObject.toBean(jo, cls);
	}

	/**
	 * ��json��ʽ���ַ�ת��Ϊĳ����������
	 * @param s
	 * @param cls
	 * @return
	 */
	public static Object getArray(String s, Class<?> cls){
		JSONArray ja=JSONArray.fromObject(s);
		return JSONArray.toArray(ja, cls);
	}

	/**
	 * ��json�ַ�ת���ɶ���
	 * @param Class<E>
	 * @param jsonStr
	 * @return E
	 */
	@SuppressWarnings({ "unchecked" })
	public static <E> E convertToObject(Class<E> c,String jsonStr){
		E e=null;
		try {
			e=c.newInstance();
			JSONObject jsonObj=JSONObject.fromObject(jsonStr);
			Map<String,Method> methodMap=getMethod(c);
			Iterator it=jsonObj.keys();
			while(it.hasNext()){
				String key=(String)it.next();
//				if("I".equals(key.substring(0, 1))){
//					key = String.valueOf((char)(key.charAt(0)+32)) + key.substring(1);
//				}
				if(methodMap.containsKey(key)){
					Method method=methodMap.get(key);
					Class paramType=method.getParameterTypes()[0];
					Object obj = jsonObj.get(key);
					if(null==String.valueOf(obj)||"null".equals(String.valueOf(obj)))
						continue;
					 if(paramType.isAssignableFrom(Object.class)){
						method.invoke(e,jsonObj.get(key));
						continue;
					} else if(paramType.isAssignableFrom(int.class)||paramType.isAssignableFrom(Integer.class)){
						method.invoke(e,jsonObj.getInt(key));
						continue;
					}else if(paramType.isAssignableFrom(double.class)||paramType.isAssignableFrom(Double.class)){
						method.invoke(e,jsonObj.getDouble(key));
						continue;
					}else if(paramType.isAssignableFrom(long.class)||paramType.isAssignableFrom(Long.class)){
						method.invoke(e,jsonObj.getLong(key));
						continue;
					}else if(paramType.isAssignableFrom(float.class)||paramType.isAssignableFrom(Float.class)){
						method.invoke(e,(Float)jsonObj.get(key));
						continue;
					}else if(paramType.isAssignableFrom(boolean.class)||paramType.isAssignableFrom(Boolean.class)){
						method.invoke(e,jsonObj.getBoolean(key));
						continue;
					}else if(paramType.isAssignableFrom(Map.class)){
						method.invoke(e,jsonObj.get(key));
						continue;
					}else if(paramType.isAssignableFrom(List.class)){
						method.invoke(e,jsonObj.get(key));
						continue;
					}

					else if(paramType.isAssignableFrom(String.class)){
						method.invoke(e, jsonObj.getString(key));
						continue;
					}
				}
			}
		}catch (Exception e1) {
			e1.printStackTrace();
		}
		return e;
	}
	/**
	 * ��ȡDTO���еķ�����
	 * @param c
	 * @return  Map<String,Method>
	 */
	private static  Map<String,Method> getMethod(Class<?> c){
		Map<String,Method> map=new HashMap<String,Method>();
		Method[] methods=c.getDeclaredMethods();
		for(int i=0;i<methods.length;i++){
			Method method=methods[i];
			if("set".equals(method.getName().substring(0, 3))){
				String methodName=method.getName().substring(3);
//				map.put(methodName.replace(methodName.charAt(0), ((char)(methodName.charAt(0)+32))), method);
				if("IDNo".equals(methodName)){
					map.put(methodName, method);
				}else{
					map.put(String.valueOf((char)(methodName.charAt(0)+32)) + methodName.substring(1), method);
				}
			}
		}
		return map;
	}
	/**
	 * ��json�ַ�ת��������
	 * @param Class<E>
	 * @param jsonStr
	 * @return List<E>
	 */
	public static <E> List<E> convertToList(Class<E> c,String jsonStr){
		List<E> list=new ArrayList<E>();
			try {
				JSONArray array=JSONArray.fromObject(jsonStr);
				for(int i=0;i<array.size();i++){
					JSONObject obj=array.getJSONObject(i);
					list.add(convertToObject(c,obj.toString()));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		return list;
	}

	public static void main(String[] args) {
		/*TemplateInstanceDto template= new TemplateInstanceDto();
		convertToObject(TemplateInstanceDto.class,JSON.encode(template));*/
	}
}
