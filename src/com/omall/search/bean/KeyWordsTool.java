package com.omall.search.bean;

/**
 *   检索筛选条件Bean
 * 
 * @ClassName:KeyWordsTool
 * @Description:检索筛选条件Bean
 * @date: 2014-12-14下午05:50:25
 * @author: 谢洪飞
 * @version: V1.0
 */
public class KeyWordsTool {

	/**检索内容*/
	private String keyWords;
	
	/**价格区间排序*/
	private String priceRangeSort;
	
	/**价格排序*/
	private String priceSort;
	
	/**价格区间较小值*/
	private String priceRangeMin;
	
	/**价格区间较大值*/
	private String priceRangeMax;
	
	/**属性筛选MapBean*/
	private String attrUtilBean;
	
	/**销量排序:只有 "销量从高到低" 排序规则*/
	private String saleCountSort;
	
	/**上市时间排序:只有"上市时间从高到低"排序规则*/
    private String saleDateSort;
    
    /**综合排序标志*/
    private String multipleSort;
    
    private String typeId;
    
    
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	public String getKeyWords() {
		return keyWords;
	}
	public void setKeyWords(String keyWords) {
		this.keyWords = keyWords;
	}
	public String getPriceRangeMin() {
		return priceRangeMin;
	}
	public void setPriceRangeMin(String priceRangeMin) {
		this.priceRangeMin = priceRangeMin;
	}
	public String getPriceRangeMax() {
		return priceRangeMax;
	}
	public void setPriceRangeMax(String priceRangeMax) {
		this.priceRangeMax = priceRangeMax;
	}
	public String getPriceSort() {
		return priceSort;
	}
	public void setPriceSort(String priceSort) {
		this.priceSort = priceSort;
	}
    
	
	public String getAttrUtilBean() {
		return attrUtilBean;
	}
	public void setAttrUtilBean(String attrUtilBean) {
		this.attrUtilBean = attrUtilBean;
	}
	public String getSaleCountSort() {
		return saleCountSort;
	}
	public void setSaleCountSort(String saleCountSort) {
		this.saleCountSort = saleCountSort;
	}
	public String getSaleDateSort() {
		return saleDateSort;
	}
	public void setSaleDateSort(String saleDateSort) {
		this.saleDateSort = saleDateSort;
	}
	public String getMultipleSort() {
		return multipleSort;
	}
	public void setMultipleSort(String multipleSort) {
		this.multipleSort = multipleSort;
	}
	public String getPriceRangeSort() {
		return priceRangeSort;
	}
	public void setPriceRangeSort(String priceRangeSort) {
		this.priceRangeSort = priceRangeSort;
	}
	
}
