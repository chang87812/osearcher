package com.omall.search.bean;

/**
 *    属性Bean
 * 
 * @ClassName:AttrUtilBean
 * @Description:属性Bean
 * @date: 2014-12-14下午05:48:25
 * @author: 谢洪飞
 * @version: V1.0
 */
public class AttrUtilBean {

	private String key;
	private String value;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
