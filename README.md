## 前言

#### 项目基于Lucene(索引技术) & IkAnalyzer(分词器技术) & CXF Web Service & Spring Mvc & Quartz 等主流开源框架搭建,为外部系统提供搜索服务;
#### 可重构代码,提供系统内部服务;


##技术架构

* Lucene 

> lucene,是Apache旗下开源的搜索引擎工具包,其实一个全文检索引擎架构;

* IKAnalyzer

> Ik Analyzer 是一个开源的，基于java语言开发的轻量级的中文分词工具包;


* CXF Web Service

> 为外部系统提供安全可用的数据访问接口;


* And so on

> ...